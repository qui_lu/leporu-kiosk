package com.leporu.leporukioskdemo;

import android.app.AlarmManager;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;

import android.support.annotation.Nullable;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Service will load at boot and poll GPS data periodically 30 minutes whenever device is running
 */

public class TrackingService extends Service {

    private LocationListener listener;
    private LocationManager locationManager;

    private String data;   // data to write to file
    private String path;   // path to save file
    private File f;        // file
    private String device_ID;    // Unique ID for device
    private String currentDate, currentDateTime, timeZone;



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        myTimer.schedule(myTask, INTERVAL);  // stop service in case it is stuck forever
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("Tracking Service", "Service started");

        Date date = new Date();
        //currentDateTime = DateFormat.getDateTimeInstance().format(date);
        currentDateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm a z").format(date);
        currentDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        File config = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/config/");
        device_ID = config.list()[0];
        f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Tracking/" + device_ID + currentDate + ".txt");
        path = f.getAbsolutePath();


        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                data = "\n" + currentDateTime +
                        " and Location: " + location.getLatitude() + " " + location.getLongitude();
                Log.d("on location changed", data);
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        try {
                            if (!f.exists()) {
                                f.createNewFile();
                            }
                            FileWriter writer = new FileWriter(f, true);
                            writer.append(data);
                            writer.flush();
                            writer.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        String content_type = getMimeType(f.getPath());

                        OkHttpClient client = new OkHttpClient.Builder()
                                .connectTimeout(10, TimeUnit.SECONDS)
                                .writeTimeout(10, TimeUnit.SECONDS)
                                .readTimeout(15, TimeUnit.SECONDS)
                                .build();

                        RequestBody file_body = RequestBody.create(MediaType.parse(content_type), f);

                        RequestBody requestBody = new MultipartBody.Builder()
                                .setType(MultipartBody.FORM)
                                .addFormDataPart("type", content_type)
                                .addFormDataPart("uploaded_file", path.substring(path.lastIndexOf("/") + 1), file_body)
                                .build();

                        Request request = new Request.Builder()
                                .url("http://138.197.204.161/save_file.php")
                                .post(requestBody)
                                .build();

                        try {
                            Response response = client.newCall(request).execute();

                            if (!response.isSuccessful()) {
                                FileWriter writer = new FileWriter(f, true);
                                writer.append(" No internet connection");
                                writer.flush();
                                writer.close();
                                Log.d("response", "in response");

                                throw new IOException("Error : " + response);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };
                thread.start();
                stopSelf();

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) { }

            @Override
            public void onProviderEnabled(String provider) { }

            @Override
            public void onProviderDisabled(String provider) { }
        };

        // net work connection available
        if (isNetworkAvailable(getApplicationContext())) {
            locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            // request network one time
            //noinspection MissingPermission
            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, listener, null);

        } else { // no internet connection
            data = "\n" + currentDateTime + " No Internet Connection";
            Thread thread = new Thread() {
                @Override
                public void run() {
                    try {
                        if (!f.exists()) {
                            f.createNewFile();
                        }
                        FileWriter writer = new FileWriter(f, true);
                        writer.append(data);
                        writer.flush();
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
            thread.start();
            stopSelf();
        }

        return Service.START_NOT_STICKY;
    }

    public static boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager)
                    context.getSystemService(context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            // if no network is available networkInfo will be null
            // otherwise check if we are connected
            return (networkInfo != null && networkInfo.isConnected());
        } catch (Exception ex) {
            return false;
        }
    }

    private String getMimeType(String path) {
        String extension = MimeTypeMap.getFileExtensionFromUrl(path);
        return MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
    }

    @Override
    public void onDestroy() {
        Log.d("Tracking Service", "Service ended");

        // set alarm to restart the service every 30 minutes
        AlarmManager alarm = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarm.set(
                alarm.RTC_WAKEUP,
                System.currentTimeMillis() + (1000 * 60 * 30), // redo this in 30 minutes
                PendingIntent.getService(this, 0, new Intent(this, TrackingService.class), 0)
        );
        super.onDestroy();

        // remove location manager
        if (locationManager != null) {
            locationManager.removeUpdates(listener);
        }

    }


    //TimerTask that will cause the run() runnable to happen.
    TimerTask myTask = new TimerTask()
    {
        public void run()
        {
            stopSelf();
        }
    };


    //Timer that will make the runnable run.
    Timer myTimer = new Timer();

    //the amount of time after which you want to stop the service
    private final long INTERVAL = 10000; // I choose 10 seconds


//    private boolean isNetworkAvailable() {
//        ConnectivityManager connectivityManager
//                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
//        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
//    }
}
