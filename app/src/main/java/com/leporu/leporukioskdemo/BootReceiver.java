package com.leporu.leporukioskdemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by qui on 5/15/2017.
 */

public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = "BootReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            context.startService(new Intent(context, TrackingService.class));
            //Log.i(TAG, "TrackingService started");
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}
