package com.leporu.leporukioskdemo;

import android.content.Context;
import android.content.ContextWrapper;
import android.media.MediaPlayer;
import android.os.Environment;
import android.util.Log;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.File;
import java.util.Arrays;

/**
 * Created by qui on 4/21/2017.
 */

public class VideoPlayer extends ContextWrapper {

    CustomVideoView customVideoView;
    String[] videoList;
    int number = 0; // counter for video
    int stopPosition; // for pause and resume

    TextView videoTitle;

    String path;

    public VideoPlayer(Context base, CustomVideoView videoView, TextView videoTitle) {
        super(base);
        customVideoView = videoView;
        this.videoTitle = videoTitle;

        path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Movies/";

        File dir = new File(path);
        videoList = dir.list();
        Arrays.sort(videoList);
        //Log.i("how many files", "file " + videoList.length);


//        customVideoView.setPlayPauseListener(new CustomVideoView.PlayPauseListener() {
//            @Override
//            public void onPlay() {
//                // System.out.println("Play!");
//            }
//
//            @Override
//            public void onPause() {
//                // System.out.println("Pause!");
//            }
//        });

        customVideoView.setVideoPath(path + videoList[0]);

        customVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // play next video
                next();
            }
        });
    }

    public void startPlaying() {
        // set video title
        String title = videoList[number].replace(".mp4", "").replace("Leporu", "Leporu:");
        //title = title.substring(5);
        videoTitle.setText(title); // set video title
        customVideoView.start();

    }

    public void pause() {
        stopPosition = customVideoView.getCurrentPosition();
        customVideoView.pause();
    }

    public void resume() {
        customVideoView.seekTo(stopPosition);
        customVideoView.start();
    }

    public void rewind() {
        customVideoView.seekTo(customVideoView.getCurrentPosition() - 3500);
    }

    public void next() {

        number++;
        if (number == videoList.length) {
            number = 0;
        }
        customVideoView.setVideoPath(path + videoList[number]);
        startPlaying();
    }

    public void previous() {
        number--;
        if (number < 0) {
            number = videoList.length - 1;
        }
        customVideoView.setVideoPath(path + videoList[number]);
        startPlaying();
    }


    public int getCurrentPosition() {
        return customVideoView.getCurrentPosition();
    }

    public int getTotalDuration() {
        return customVideoView.getDuration();
    }

    public void seekTo(int location) {
        customVideoView.seekTo(location);
    }

}
