package com.leporu.leporukioskdemo;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;


/**
 * Created by qui on 4/27/2017.
 * AdminReceiver
 */
public class AdminReceiver extends DeviceAdminReceiver {

    @Override
    public void onEnabled(Context context, Intent intent) {
        Common.showToast(context, "[Device Admin Enabled]");
        Common.becomeHomeActivity(context);
    }

    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        return "Warning: Device Admin is going to be disabled.";
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        Common.showToast(context, "[Device Admin Disabled]");
    }

    @Override
    public void onLockTaskModeEntering(Context context, Intent intent,
                                       String pkg) {
        Common.showToast(context, "[Kiosk Mode Enabled]");
    }

    @Override
    public void onLockTaskModeExiting(Context context, Intent intent) {
        Common.showToast(context, "[Kiosk Mode Disabled]");
    }
}