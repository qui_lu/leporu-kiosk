package com.leporu.leporukioskdemo;

import android.Manifest;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;

import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {
    private final static String TAG = "KioskModeDemo";
    private ImageButton buttonRewind, buttonPrevious, buttonNext;
    private SeekBar videoProgressBar;

    private TextView videoCurrentDurationLabel;
    private TextView videoTotalDurationLabel;
    private TextView videoTitle;

    private boolean inKioskMode = false;
    private DevicePolicyManager dpm;
    private ComponentName deviceAdmin;

    private CustomVideoView customVideoView;
    private VideoPlayer videoPlayer;
    private View decorView;
    private RelativeLayout mLayout = null;

    private OnScreenOffReceiver onScreenOffReceiver;

    private int lock = 0;

    private Handler mHandler = new Handler();
    private Utilities utils;
    private FrameLayout footerLayout;
    private LinearLayout headerLayout;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // get all necessary permissions
        getPermissions();

        utils = new Utilities();

        mLayout = (RelativeLayout) findViewById(R.id.mLayout);

        mLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                checkTouch(event);
                return true;
            }
        });


        // set up buttons
        footerLayout = (FrameLayout) findViewById(R.id.player_footer_bg);
        buttonPrevious = (ImageButton) findViewById(R.id.buttonPrevious);
        buttonNext = (ImageButton) findViewById(R.id.buttonNext);
        videoProgressBar = (SeekBar) findViewById(R.id.videoSeekBar);
        videoCurrentDurationLabel = (TextView) findViewById(R.id.videoCurrentDurationLabel);
        videoTotalDurationLabel = (TextView) findViewById(R.id.videoTotalDurationLabel);

        headerLayout = (LinearLayout) findViewById(R.id.player_header_bg);
        videoTitle = (TextView) findViewById(R.id.video_title);

        videoProgressBar.setOnSeekBarChangeListener(this);


        // disable turn screen off function
        registerKioskModeScreenOffReceiver();

        // Turn on Immersive mode for application
        decorView = getWindow().getDecorView();
        hideSystemUI();

        // Lock application to Kiosk mode
        deviceAdmin = new ComponentName(this, AdminReceiver.class);
        dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (!dpm.isAdminActive(deviceAdmin)) {
            showToast("Error: Missing Device Admin!");
        }
        if (dpm.isDeviceOwnerApp(getPackageName())) {
            dpm.setLockTaskPackages(deviceAdmin,
                    new String[]{getPackageName()});
        } else {
            showToast("Error: Missing Device Owner!");
        }
        // lock tablet to Kiosk Mode
        setKioskMode(true);


        // initialize video player
        customVideoView = (CustomVideoView) findViewById(R.id.videoView);
        customVideoView.setZ(-10.0f);
        videoPlayer = new VideoPlayer(this, customVideoView, videoTitle);


        // start video right away
        playVideo();
        hideSystemUI();

    }


    private void getPermissions() {
        // set wifi to true
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);

        // get GPS permissions
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[2] == PackageManager.PERMISSION_GRANTED && grantResults[3] == PackageManager.PERMISSION_GRANTED) {
                // we are good
            } else {
                getPermissions(); // re-ask for permissions
            }
        }
    }

    // Register receiver to turn screen back on when user taps power button
    private void registerKioskModeScreenOffReceiver() {
        // register screen off receiver
        final IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_OFF);
        onScreenOffReceiver = new OnScreenOffReceiver();
        registerReceiver(onScreenOffReceiver, filter);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
        // show control buttons when focus is changed
        showControls();
    }

    @Override
    public void onPause() {
        super.onPause();
        videoPlayer.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        showControls();

        // re-lock kiosk mode upon resuming application
        if (!inKioskMode) {
            setKioskMode(true);
        }
        // resume playing video
        videoPlayer.resume();
    }

    public void hideSystemUI() {
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    private void showToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }


    /* -------------------- KIOSK CONTROL ------------------------ */

    private void checkTouch(MotionEvent event) {
        // show control buttons when screen is touched
        showControls();

        int x = (int) event.getX();
        int y = (int) event.getY();

        int rangeX1 = 250, rangeY1 = 200;
        int rangeX2 = 1000, rangeY2 = 200;
        int rangeX3 = 250, rangeY3 = 600;
        int rangeX4 = 1000, rangeY4 = 600;

        switch (lock) {
            case 0:
                if (x > 0 && x < rangeX1 && y > 0 && y < rangeY1) {
                    lock = 1;
                    //showToast("1");
                }
                break;
            case 1:
                if (x > rangeX2 && x < 1280 && y > 0 && y < rangeY2) {
                    lock = 2;
                } else if (!(x > 0 && x < rangeX1 && y > 0 && y < rangeY1)) {
                    lock = 0;
                    //showToast("1 to 0");
                }
                break;

            case 2:
                if (x > 0 && x < rangeX3 && y > rangeY3 && y < 800) {
                    lock = 3;
                } else if (!(x > rangeX2 && x < 1280 && y > 0 && y < rangeY2)) {
                    lock = 0;
                    //showToast("2 to 0");
                }
                break;

            case 3:
                if (x > rangeX4 && x < 1280 && y > rangeY4 && y < 800) {
                    lock = 4;
                } else if (!(x > 0 && x < rangeX3 && y > rangeY3 && y < 800)) {
                    lock = 0;
                    //showToast("3 to 0");
                }
                break;
            case 4:
                if (x > 0 && x < rangeX1 && y > 0 && y < rangeY1) {
                    lock = 0;
                    setKioskMode(false);
                } else if (!(x > rangeX4 && x < 1280 && y > rangeY4 && y < 800)) {
                    lock = 0;
                    //showToast("4 to 0");
                }
        }
    }

    private void showControls() {
        videoProgressBar.setVisibility(View.VISIBLE);
        buttonNext.setVisibility(View.VISIBLE);
        buttonPrevious.setVisibility(View.VISIBLE);
        footerLayout.setVisibility(View.VISIBLE);
        videoCurrentDurationLabel.setVisibility(View.VISIBLE);
        videoTotalDurationLabel.setVisibility(View.VISIBLE);
        headerLayout.setVisibility(View.VISIBLE);
        videoTitle.setVisibility(View.VISIBLE);

        buttonPrevious.postDelayed(new Runnable() {
            @Override
            public void run() {
//                // fade out animation
//                Animation fadeOut = new AlphaAnimation(1, 0);
//                fadeOut.setInterpolator(new AccelerateInterpolator());
//                fadeOut.setStartOffset(3500);
//                fadeOut.setDuration(600);
//
//                AnimationSet animation = new AnimationSet(false);
//                animation.addAnimation(fadeOut);
//
//                videoProgressBar.setAnimation(animation);
//                buttonNext.setAnimation(animation);
//                buttonPrevious.setAnimation(animation);
//                footerLayout.setAnimation(animation);
//                videoCurrentDurationLabel.setAnimation(animation);
//                videoTotalDurationLabel.setAnimation(animation);
//                headerLayout.setAnimation(animation);
//                videoTitle.setAnimation(animation);

                // translate animation
                TranslateAnimation translateBottom = new TranslateAnimation(0, 0, 0, footerLayout.getHeight());
                translateBottom.setStartOffset(3500);
                translateBottom.setDuration(600);
                //translateBottom.setFillAfter(true);
                videoProgressBar.startAnimation(translateBottom);
                buttonNext.startAnimation(translateBottom);
                buttonPrevious.startAnimation(translateBottom);
                footerLayout.startAnimation(translateBottom);
                videoCurrentDurationLabel.startAnimation(translateBottom);
                videoTotalDurationLabel.startAnimation(translateBottom);

                TranslateAnimation translateTop = new TranslateAnimation(0, 0, 0, -(headerLayout.getHeight()));
                translateTop.setStartOffset(3500);
                translateTop.setDuration(600);
                //translateTop.setFillAfter(true);
                headerLayout.startAnimation(translateTop);
                videoTitle.startAnimation(translateTop);

                footerLayout.setVisibility(View.GONE);
                videoProgressBar.setVisibility(View.GONE);
                buttonNext.setVisibility(View.GONE);
                buttonPrevious.setVisibility(View.GONE);
                videoCurrentDurationLabel.setVisibility(View.GONE);
                videoTotalDurationLabel.setVisibility(View.GONE);
                headerLayout.setVisibility(View.GONE);
                videoTitle.setVisibility(View.GONE);
            }
        }, 10);

    }

    private void setKioskMode(boolean on) {
        try {
            if (on) {
                if (dpm.isLockTaskPermitted(this.getPackageName())) {
                    startLockTask();
                    inKioskMode = true;
                    Common.becomeHomeActivity(this);
                } else {
                    showToast("Error: Kiosk Mode Not Permitted");
                }
            } else {
                stopLockTask();
                inKioskMode = false;
                dpm.clearPackagePersistentPreferredActivities(deviceAdmin,
                        this.getPackageName());
                showToast("Home activity: " + Common.getHomeActivity(this));
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e);
        }
    }



    /* ------------------- VIDEO PLAYER ------------------- */
    private void playVideo() {
        videoPlayer.startPlaying();
        videoProgressBar.setProgress(0);
        videoProgressBar.setMax(100);
        updateProgressBar();
    }


    public void onNextClick(View view) {
        showControls();
        videoPlayer.next();
    }

    public void onPreviousClick(View view) {
        showControls();
        videoPlayer.previous();
    }

    /**
     * Update timer on seekbar
     * */
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * Background Runnable thread to update time on progress bar
     * */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            int totalDuration = videoPlayer.getTotalDuration();
            int currentDuration = videoPlayer.getCurrentPosition();

            // Displaying Total Duration time
            videoTotalDurationLabel.setText(""+ utils.milliSecondsToTimer(totalDuration));
            // Displaying time completed playing
            videoCurrentDurationLabel.setText(""+ utils.milliSecondsToTimer(currentDuration));

            // Updating progress bar
            int progress = utils.getProgressPercentage(currentDuration, totalDuration);

            videoProgressBar.setProgress(progress);

            // Running this thread again after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    /**
     *  Unused method (replace by 2 methods below)
     * */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {

    }

    /**
     * When user starts moving the progress handler
     * */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        mHandler.removeCallbacks(mUpdateTimeTask);
        showControls();
    }

    /**
     * When user stops moving the progress handler
     * */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = videoPlayer.getTotalDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

        // forward or backward to certain seconds
        videoPlayer.seekTo(currentPosition);

        // update timer progress again
        updateProgressBar();
    }
}